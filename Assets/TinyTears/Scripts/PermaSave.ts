
namespace game {

    /** New System */
    export class PermaSave extends ut.ComponentSystem {


        //CONSTANTLY SAVE
        OnUpdate():void 
        {            
            this.world.forEach([GameData],
                (data) => {
                    if (data.CurrentTears == "!")
                    {
                        data.CurrentTears = DataManager.getCookie("TotalTears");
                    }
                    else
                    {
                        DataManager.setCookie("TotalTears",data.CurrentTears);
                    }
                    //For debuggin, increment by 1.
                    //data.CurrentTears = BigInt.bigInt2str(BigInt.addInt(BigInt.str2bigInt(data.CurrentTears,DataManager.BASE),1),DataManager.BASE);
                });
        }

    }
}
