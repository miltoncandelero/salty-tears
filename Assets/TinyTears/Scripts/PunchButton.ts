
namespace game {

    /** New System */
    export class PunchButton extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([ut.UIControls.Button, ut.UIControls.MouseInteraction, TagPunchButton],
                (button, mouseInteraction) => {
                    
                    if (mouseInteraction.clicked)
                    {
                        this.world.forEach([GameData],
                            (data) => {
                                data.CurrentTears = BigInt.bigInt2str(BigInt.add(BigInt.str2bigInt(data.CurrentTears,DataManager.BASE),BigInt.str2bigInt(data.PunchStrenght,DataManager.BASE)),DataManager.BASE);
                            });
                    }
                    

                });
        }
    }
}
