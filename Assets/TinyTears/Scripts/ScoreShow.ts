
namespace game {

    /** New System */
    export class ScoreShow extends ut.ComponentSystem {
        
        OnUpdate():void {
            this.world.forEach([ut.Text.Text2DRenderer,TagScoreShow],
                (text,link) => {

                    this.world.forEach([GameData],
                        (data) => {
                            text.text = BigInt.bigInt2str(BigInt.str2bigInt(data.CurrentTears,DataManager.BASE),10);
                        });
                });
        }
    }
}
